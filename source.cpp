#include <stdio.h>

int bsearch(int a[], int x, int l, int r)  //array, search number, left border (0 for default), right border (array size for default)
{
 int temp = x + 1;                        // cuz we search for x+, not for x.
 int mid = (l + r) / 2;
 
 if(r - l == 1)                           //recurtion exit
 {
  if(a[mid+1] <= x)
  {
   return -1;  //no solution
  }
  else
  {
   return mid+1;
  }
 }

 if(a[mid]>= temp)                      //standard binsearch
 {
  r = mid;
 }
 else
 {
  l = mid;
 }

 return bsearch(a, x, l, r);
}

int main()
{
 int a[] = {0, 1, 1, 2, 2, 5};            //test array
 int x = 3;                               //search for x+

 printf("%d\n", bsearch(a, x, 0, 6));
 return 0;
}